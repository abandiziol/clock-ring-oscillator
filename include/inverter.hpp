#ifndef INVERTER_HPP
#define INVERTER_HPP

#include <systemc.h>
#include <iostream>

SC_MODULE(Inverter){

	//Declaration of the inputs
	sc_in<bool> input;

	//Declaration of the outputs
	sc_out<bool> output;

	//Methods Declaration
	void Execute();

	//Constructor
	SC_CTOR(Inverter) {

		SC_THREAD(Execute);
			sensitive << input;
	}	

};//End of the Register File Module

#endif
