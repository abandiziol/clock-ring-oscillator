#ifndef OR_HPP
#define OR_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Or_port)
{
   sc_in<bool> A;
   sc_in<bool> B;
   sc_out<bool> S;

   void do_or()
   {
      S = ( A->read() | B->read() );
   }

   SC_CTOR(Or_port) {
      SC_METHOD(do_or); 
      sensitive << A << B; 
   }

};

#endif
