#include <systemc.h>

SC_MODULE(Testbench_Stimuli_Inverter){

	//Declaration of the outputs
	sc_out<bool> input;

	//Declaration of the inputs
	sc_in<bool> output;
	sc_in<bool> clock;

	void stimgen();

	SC_CTOR(Testbench_Stimuli_Inverter){

		SC_THREAD(stimgen);
		sensitive_pos << clock;
	}

};
