#ifndef RING_STIMULUS_HPP
#define RING_STIMULUS_HPP

#include <systemc.h>

SC_MODULE(Ring_Stimulus){

        //Declaration of the inputs
        sc_in<bool> clock;

        //Declaration of the outputs
        sc_out<bool> reset;


        void stimgen();

        SC_CTOR(Ring_Stimulus){

                SC_THREAD(stimgen);
                sensitive << clock; 

        }

};

#endif

