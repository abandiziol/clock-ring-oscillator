// Ring Oscillator:

#ifndef RING_HPP
#define RING_HPP

#include <systemc.h>
#include "inverter.hpp"
#include "or_port.hpp"

using namespace std;

SC_MODULE(Ring)
{
   sc_in<bool> reset;
   sc_out<bool> clock;

   // internal signals
   sc_signal<bool> or_out;
   sc_signal<bool> inv1_out;
   sc_signal<bool> inv2_out;
   sc_signal<bool> inv3_out;

   Inverter inv1;
   Inverter inv2;
   Inverter inv3;
   Inverter inv_exit;
   Or_port or1;

   Ring(sc_module_name name);

};



#endif
