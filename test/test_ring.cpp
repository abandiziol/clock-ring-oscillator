#include "systemc.h"
#include <iostream>
#include "testbench_stimuli_ring.hpp"
#include "ring.hpp"

int sc_main (int argc, char* argv[]) {

	sc_signal<bool> reset;
	sc_signal<bool> clock;
	
	Ring_Stimulus stim1("Ring_stimulus");

	stim1.clock(clock);
	stim1.reset(reset);

	Ring ring1("Ring");
		
	ring1.reset(reset);
	ring1.clock(clock);

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("Ring");

	//Dump the desired signals
	sc_trace(wf, clock, "clock");
	sc_trace(wf, reset, "reset");


	//sc_start(SC_ZERO_TIME);
	sc_start(50, SC_NS);

//	cout << "Simulazione lanciata" << endl;

	sc_close_vcd_trace_file(wf);
	return 0;

}
