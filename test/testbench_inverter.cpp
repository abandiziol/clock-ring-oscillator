#include "systemc.h"
#include <iostream>
#include "testbench_stimuli_inverter.hpp"
#include "inverter.hpp"




int sc_main (int argc, char* argv[]) {

	sc_signal<bool> input1;
	sc_signal<bool> output1;
	sc_signal<bool> output2;
	sc_signal<bool> output3;
	sc_clock clock("clock");
	
	Testbench_Stimuli_Inverter t1("Testbench_Stimuli_Inverter");

	t1.input(input1);
	t1.output(output3);
	t1.clock(clock);

	Inverter i1("Inverter1");
		
	i1.input(input1);
	i1.output(output1);
	
	Inverter i2("Inverter2");
		
	i2.input(output1);
	i2.output(output2);
	
	Inverter i3("Inverter3");
		
	i3.input(output2);
	i3.output(output3);

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("Inverter");

	//Dump the desired signals
	sc_trace(wf, input1, "input1");
	sc_trace(wf, output1, "output1");
	sc_trace(wf, output2, "output2");	
	sc_trace(wf, output3, "output3");
	sc_trace(wf, clock, "clock");


	sc_start(SC_ZERO_TIME);
	sc_start(50, SC_NS);

	sc_close_vcd_trace_file(wf);
	return 0;

}
