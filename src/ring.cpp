// alu

#include <systemc.h>
#include "ring.hpp"

using namespace std;

SC_HAS_PROCESS(Ring);

Ring::Ring(sc_module_name name) :
   sc_module(name), or1("or1"), inv1("inv1"), inv2("inv2"), inv3("inv3"), inv_exit("inv_exit")
{
   or1.A(this->inv3_out);
   or1.B(this->reset);
   or1.S(this->or_out);
   inv1.input(this->or_out);
   inv1.output(this->inv1_out);
   inv2.input(this->inv1_out);
   inv2.output(this->inv2_out);
   inv3.input(this->inv2_out);
   inv3.output(this->inv3_out);
   inv_exit.input(this->inv3_out);
   inv_exit.output(this->clock);
}

